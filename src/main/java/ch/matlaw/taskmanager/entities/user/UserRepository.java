package ch.matlaw.taskmanager.entities.user;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Tamino Walter
 * @version ApplicationUserRepository_07.2020
 * @since 08.07.2020
 */
public interface UserRepository extends JpaRepository<User, Long> {
    User findByUsername(String username);
}
