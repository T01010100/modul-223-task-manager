package ch.matlaw.taskmanager.entities;

import ch.matlaw.taskmanager.entities.subtask.SubTask;
import ch.matlaw.taskmanager.entities.subtask.SubTaskRepository;
import ch.matlaw.taskmanager.entities.task.Task;
import ch.matlaw.taskmanager.entities.task.TaskRepository;
import ch.matlaw.taskmanager.entities.todolist.ToDoList;
import ch.matlaw.taskmanager.entities.todolist.ToDoListRepository;
import ch.matlaw.taskmanager.entities.user.UserRepository;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import java.util.List;

/**
 * @author Tamino Walter
 * @version EntityService_07.2020
 * @since 10.07.2020 13:06
 */
@Service
public class EntityService {

    private UserRepository userRepo;
    private ToDoListRepository todoRepo;
    private TaskRepository taskRepo;
    private SubTaskRepository subTaskRepo;

    public EntityService(UserRepository userRepo, ToDoListRepository todoRepo, TaskRepository taskRepo, SubTaskRepository subTaskRepo) {
        this.userRepo = userRepo;
        this.todoRepo = todoRepo;
        this.taskRepo = taskRepo;
        this.subTaskRepo = subTaskRepo;
    }

    /**
     * add new to-do list
     * @param list data
     */
    public void addToDoList(ToDoList list) {
        todoRepo.save(list);
    }

    /**
     * get to-do lists
     * @return list of to-do lists
     */
    public List<ToDoList> getToDoLists() {
        return todoRepo.findAll();
    }

    /**
     * edit existing to-do list
     * @param id of list
     * @param list data
     */
    public void editToDoList(long id, ToDoList list) {
        ToDoList existingToDo = todoRepo.findById(id).get();
        Assert.notNull(existingToDo, "Task not found");
        existingToDo.setDescription(list.getDescription());
        todoRepo.save(existingToDo);
    }

    /**
     * delete existing to-do list
     * @param id of list
     */
    public void deleteToDoList(long id) {
        ToDoList todoToDel = todoRepo.findById(id).get();
        todoRepo.delete(todoToDel);
    }

    /**
     * get to-do list by description (special request)
     * @param description of list
     * @return list
     */
    public ToDoList getToDoListByDescription(String description) {
        return todoRepo.findByDescription(description);
    }

    /**
     * add new task
     * @param task data
     */
    public void addTask(Task task) {
        taskRepo.save(task);
    }

    /**
     * add new task to a to-do list
     * @param task data
     * @param id of to-do list
     */
    public void addTaskToList(Task task, long id) {
        ToDoList target = todoRepo.findById(id).get();
        Assert.notNull(target, "List not found");
        task.setToDoList(target);
        taskRepo.save(task);
    }

    /**
     * get all tasks
     * @return all tasks
     */
    public List<Task> getTasks() {
        return taskRepo.findAll();
    }

    /**
     * edit task
     * @param id od task
     * @param task data
     */
    public void editTask(long id, Task task) {
        Task existingTask = taskRepo.findById(id).get();
        Assert.notNull(existingTask, "Task not found");
        existingTask.setDescription(task.getDescription());
        existingTask.setCompleted(task.isCompleted());
        taskRepo.save(existingTask);
    }

    /**
     * delete task
     * @param id of task
     */
    public void deleteTask(long id) {
        Task taskToDel = taskRepo.findById(id).get();
        taskRepo.delete(taskToDel);
    }

    /**
     * add sub task
     * @param subTask data
     */
    public void addSubTask(SubTask subTask) {
        subTaskRepo.save(subTask);
    }

    /**
     * get all sub tasks
     * @return all sub tasks
     */
    public List<SubTask> getSubTasks() {
        return subTaskRepo.findAll();
    }

    /**
     * edit sub task
     * @param id of sub task
     * @param subTask data
     */
    public void editSubTask(long id, SubTask subTask) {
        SubTask existingSubTask = subTaskRepo.findById(id).get();
        Assert.notNull(existingSubTask, "SubTask not found");
        existingSubTask.setDescription(subTask.getDescription());
        existingSubTask.setCompleted(subTask.isCompleted());
        subTaskRepo.save(existingSubTask);
    }

    /**
     * delete sub task
     * @param id of sub task
     */
    public void deleteSubTask(long id) {
        SubTask taskToDel = subTaskRepo.findById(id).get();
        subTaskRepo.delete(taskToDel);
    }
}
