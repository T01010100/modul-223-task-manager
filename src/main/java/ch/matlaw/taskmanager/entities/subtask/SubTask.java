package ch.matlaw.taskmanager.entities.subtask;

import ch.matlaw.taskmanager.entities.task.Task;
import com.fasterxml.jackson.annotation.JsonBackReference;

import javax.persistence.*;

/**
 * @author Tamino Walter
 * @version SubTask_07.2020
 * @since 10.07.2020 09:33
 */
@Entity
public class SubTask {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private boolean completed;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="task_id", referencedColumnName = "id")
    private Task task;

    protected SubTask() {
        completed = false;
    }

    public SubTask(String description, Task task) {
        super();
        this.description = description;
        this.task = task;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Task getTask() {
        return task;
    }

    public void setTask(Task task) {
        this.task = task;
    }
}
