package ch.matlaw.taskmanager.entities.subtask;

import ch.matlaw.taskmanager.entities.EntityService;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Tamino Walter
 * @version SubTaskController_07.2020
 * @since 10.07.2020 09:34
 */
@RestController
@RequestMapping("/subtasks")
public class SubTaskController {

    private EntityService service;

    public SubTaskController(EntityService service) {
        this.service = service;
    }

    @PostMapping
    public void addSubTask(@RequestBody SubTask task) {
        service.addSubTask(task);
    }

    @GetMapping
    public List<SubTask> getSubTasks() {
        return service.getSubTasks();
    }

    @PutMapping("/{id}")
    public void editSubTask(@PathVariable long id, @RequestBody SubTask task) {
        service.editSubTask(id, task);
    }

    @DeleteMapping("/{id}")
    public void deleteSubTask(@PathVariable long id) {
        service.deleteSubTask(id);
    }
}
