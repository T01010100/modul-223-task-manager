package ch.matlaw.taskmanager.entities.subtask;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Tamino Walter
 * @version SubTaskRepository_07.2020
 * @since 10.07.2020 09:34
 */
public interface SubTaskRepository extends JpaRepository<SubTask, Long> {
}
