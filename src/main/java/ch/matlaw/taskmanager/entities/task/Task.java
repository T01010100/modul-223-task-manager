package ch.matlaw.taskmanager.entities.task;

import ch.matlaw.taskmanager.entities.subtask.SubTask;
import com.fasterxml.jackson.annotation.JsonBackReference;
import ch.matlaw.taskmanager.entities.todolist.ToDoList;
import com.fasterxml.jackson.annotation.JsonManagedReference;

import javax.persistence.*;
import java.util.List;

/**
 * @author Tamino Walter
 * @since 09.07.2020
 */
@Entity
public class Task {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;
    private boolean completed;

    @JsonBackReference
    @ManyToOne
    @JoinColumn(name="toDoList_id", referencedColumnName = "id")
    private ToDoList toDoList;

    @JsonManagedReference
    @OneToMany(mappedBy = "task")
    private List<SubTask> subTasks;

    protected Task() {
        this.completed = false;
    }

    public Task(String description, ToDoList toDoList) {
        super();
        this.description = description;
        this.toDoList = toDoList;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getId() {
        return id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ToDoList getToDoList() {
        return toDoList;
    }

    public void setToDoList(ToDoList toDoList) {
        this.toDoList = toDoList;
    }

    public boolean isCompleted() {
        return completed;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }
}