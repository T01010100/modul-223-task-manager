package ch.matlaw.taskmanager.entities.task;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Tamino Walter
 * @since 09.07.2020
 */
public interface TaskRepository extends JpaRepository<Task, Long> {
}