package ch.matlaw.taskmanager.entities.task;

import ch.matlaw.taskmanager.entities.EntityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Tamino Walter
 * @since 09.07.2020
 */
@RestController
@RequestMapping("/tasks")
public class TaskController {

    private EntityService service;

    public TaskController(EntityService service) {
        this.service = service;
    }

    @PostMapping
    public void addTask(@RequestBody Task task) {
        service.addTask(task);
    }

    @PostMapping("/{todoId}")
    public void addTaskToList(@RequestBody Task task, @PathVariable long todoId) {
        service.addTaskToList(task, todoId);
    }

    @GetMapping
    public List<Task> getTasks() {
        return service.getTasks();
    }

    @PutMapping("/{id}")
    public void editTask(@PathVariable long id, @RequestBody Task task) {
        service.editTask(id, task);
    }

    @DeleteMapping("/{id}")
    public void deleteTask(@PathVariable long id) {
        service.deleteTask(id);
    }
}