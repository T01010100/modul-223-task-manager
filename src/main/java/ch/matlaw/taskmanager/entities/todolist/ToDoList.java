package ch.matlaw.taskmanager.entities.todolist;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import ch.matlaw.taskmanager.entities.task.Task;

import javax.persistence.*;

import java.util.ArrayList;
import java.util.List;

/**
 * @author Tamino Walter
 * @version ToDoList_07.2020
 * @since 09.07.2020
 */
@Entity
public class ToDoList {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private String description;

    @JsonManagedReference
    @OneToMany(mappedBy = "toDoList")
    private List<Task> tasks;

    protected ToDoList() { }

    public ToDoList(String description) {
        this.description = description;
        this.tasks = new ArrayList<>();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<Task> getTasks() {
        return tasks;
    }

    public void setTasks(List<Task> tasks) {
        this.tasks = tasks;
    }
}
