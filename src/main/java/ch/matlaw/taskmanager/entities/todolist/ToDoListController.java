package ch.matlaw.taskmanager.entities.todolist;

import ch.matlaw.taskmanager.entities.EntityService;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author Tamino Walter
 * @version ToDoListController_07.2020
 * @since 09.07.2020
 */
@RestController
@RequestMapping("/todolists")
public class ToDoListController {

    private EntityService service;

    public ToDoListController(EntityService service) {
        this.service = service;
    }

    @PostMapping
    public void addToDo(@RequestBody ToDoList toDoList) {
        service.addToDoList(toDoList);
    }

    @GetMapping
    public List<ToDoList> getToDos() {
        return service.getToDoLists();
    }

    @PutMapping("/{id}")
    public void editToDo(@PathVariable long id, @RequestBody ToDoList list) {
        service.editToDoList(id, list);
    }

    @DeleteMapping("/{id}")
    public void deleteToDo(@PathVariable long id) {
        service.deleteToDoList(id);
    }

    @GetMapping("/{description}")
    public ToDoList getToDoByDescription(@PathVariable String description) {
        return service.getToDoListByDescription(description);
    }
}
