package ch.matlaw.taskmanager.entities.todolist;

import org.springframework.data.jpa.repository.JpaRepository;

/**
 * @author Tamino Walter
 * @version ToDoListRepository_07.2020
 * @since 09.07.2020
 */
public interface ToDoListRepository extends JpaRepository<ToDoList, Long> {
    ToDoList findByDescription(String description);
}
