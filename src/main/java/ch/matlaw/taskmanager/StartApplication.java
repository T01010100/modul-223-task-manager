package ch.matlaw.taskmanager;

import ch.matlaw.taskmanager.entities.subtask.SubTask;
import ch.matlaw.taskmanager.entities.subtask.SubTaskRepository;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import ch.matlaw.taskmanager.entities.task.Task;
import ch.matlaw.taskmanager.entities.task.TaskRepository;
import ch.matlaw.taskmanager.entities.todolist.ToDoList;
import ch.matlaw.taskmanager.entities.todolist.ToDoListRepository;

/**
 * @author Tamino Walter
 * @since 09.07.2020
 */
@SpringBootApplication
public class StartApplication {

	@Bean
	public BCryptPasswordEncoder bCryptPasswordEncoder() {
		return new BCryptPasswordEncoder();
	}

	@Bean
	public CommandLineRunner demoData(ToDoListRepository todoRepo, TaskRepository taskRepo, SubTaskRepository subTaskRepo) {
		return (args -> {
			// TODO List 1
			ToDoList l1 = new ToDoList("buy this food");
			todoRepo.save(l1);

			Task t1 = new Task("buy watermelons", l1);
			taskRepo.save(t1);

			SubTask st1 = new SubTask("big watermelon", t1);
			subTaskRepo.save(st1);
			SubTask st2 = new SubTask("small watermelon", t1);
			subTaskRepo.save(st2);

			// TODO List 2
			ToDoList l2 = new ToDoList("homework");
			todoRepo.save(l2);

			Task t2 = new Task("german", l2);
			taskRepo.save(t2);
			Task t3 = new Task("english", l2);
			taskRepo.save(t3);
		});
	}

	public static void main(String[] args) {
		SpringApplication.run(StartApplication.class, args);
	}
}
